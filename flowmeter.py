# -*- coding: utf-8 -*-
import time


class FlowMeter:
    MS_IN_A_SECOND = 1000.0
    enabled = True
    pulses_per_liter = 0.0
    clicks = 0
    lastClick = 0
    clickDelta = 0
    hertz = 0.0
    flow = 0  # in Liters per second
    thisPour = 0.0  # in Liters
    totalPour = 0.0  # in Liters
    is_under_maintenance = False
    GPIO_pin = 0
    valve_GPIO_pin = 0
    valve_opened = False

    price = 0.0

    def __init__(self, pulses_per_liter):
        self.lastClick = int(time.time() * FlowMeter.MS_IN_A_SECOND)
        self.pulses_per_liter = pulses_per_liter
        self.price = 0.0

    def set_price(self, price):
        self.price = price

    def set_valve_GPIO_pin(self, valve_GPIO_pin):
        self.valve_GPIO_pin = valve_GPIO_pin

    def set_maintenance(self, is_under_maintenance):
        self.is_under_maintenance = is_under_maintenance

    def tick(self):
        if not self.enabled:
            return
        current_time = int(time.time() * FlowMeter.MS_IN_A_SECOND)
        self.clicks += 1
        # get the time delta
        self.clickDelta = max((current_time - self.lastClick), 1)
        if self.enabled == True and self.clickDelta < 1000:
            inst_pour = 1 / self.pulses_per_liter
            self.thisPour += inst_pour
            self.totalPour += inst_pour
        # Update the last click
        self.lastClick = current_time

    def clear(self):
        self.thisPour = 0;
        self.totalPour = 0;
        self.clicks = 0;
