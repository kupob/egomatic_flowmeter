# -*- coding: utf-8 -*-
# !/usr/bin/python
import threading
import RPi.GPIO as GPIO
from flowmeter import *
from network_sender import *
from network_receiver import *
from configreader import *
import ast
from cyrillic_converter import *
import logging

import Adafruit_CharLCD as LCD

logging.basicConfig(filename='/tmp/egomatic_flowmeter{0}.log'.format(time.time()), level=logging.DEBUG)

logging.debug("Egomatic flowmeter system started")

balance_limit = 5.0

# read configuration settings
config = ConfigReader()
_display_GPIO_pins = [int(s) for s in config.get_display_GPIO_pins().split()]
_flowmeters_GPIO_pins = [int(s) for s in config.get_flowmeters_GPIO_pins().split()]
_valves_GPIO_pins = [int(s) for s in config.get_valves_GPIO_pins().split()]

GPIO.setmode(GPIO.BCM)  # use real GPIO numbering
GPIO.setup(_flowmeters_GPIO_pins, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(_valves_GPIO_pins, GPIO.OUT, initial=GPIO.HIGH)

logging.debug('Listening flowmeters on pins ' + str(_flowmeters_GPIO_pins).strip('[]'))

lcd_columns = 16
lcd_rows = 2

(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7) = _display_GPIO_pins
lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_columns, lcd_rows)
lcd.clear()

# set up the flow meters
flowmeters = {}
prices_loaded = False
customer_balance = 0.0
new_customer_balance = 0.0
customer_id = None


def tick_once(channel):
    flowmeters[channel].tick()


def lock_valve(flowmeter):
    if flowmeter.valve_GPIO_pin and flowmeter.valve_opened:
        GPIO.output(flowmeter.valve_GPIO_pin, GPIO.HIGH)
        flowmeter.valve_opened = False


def unlock_valve(flowmeter):
    if flowmeter.valve_GPIO_pin and not flowmeter.valve_opened:
        GPIO.output(flowmeter.valve_GPIO_pin, GPIO.LOW)
        flowmeter.valve_opened = True


def send_flow(flowmeter_pin):
    logging.debug("SEND BALANCE")
    flowmeter = flowmeters[flowmeter_pin]
    logging.debug(u"{} {} {}".format(flowmeter.totalPour, customer_balance, new_customer_balance))
    sender.send_flow(flowmeter_pin, flowmeter.totalPour, customer_balance, new_customer_balance, customer_id)
    sender_event.set()


i = 0
for pin in _flowmeters_GPIO_pins:
    _flowmeter = FlowMeter(config.get_pulses_per_liter())
    _flowmeter.GPIO_pin = pin
    flowmeters[pin] = _flowmeter
    GPIO.add_event_detect(pin, GPIO.RISING, callback=tick_once, bouncetime=20)

sender_event = threading.Event()
sender = NetworkSender(sender_event)
sender.daemon = True
sender.start()
receiver_event = threading.Event()
receiver = NetworkReceiver(receiver_event)
receiver.daemon = True
receiver.start()

lcd_string = ''

# main loop
last_notify_time = 0
last_lcd_update_time = 0
last_acton_time = 0
current_used_flowmeter = 0
while True:
    current_time = int(time.time() * FlowMeter.MS_IN_A_SECOND)

    # Notify periodically
    if current_time - last_notify_time > 4000:
        for pin in _flowmeters_GPIO_pins:
            flowmeter = flowmeters[pin]
        sender_event.set()
        sender.send_notify_signal()
        last_notify_time = current_time
        if not prices_loaded:
            sender_event.set()
            sender.send_prices_request()
        lcd_string = u''

    totalPour = 0.0

    for pin in _flowmeters_GPIO_pins:
        flowmeter = flowmeters[pin]

        if flowmeter.is_under_maintenance:
            unlock_valve(flowmeter)
            continue

        if (current_used_flowmeter and current_used_flowmeter != pin) or \
           (not customer_id) or \
           (balance_limit > new_customer_balance):
            lock_valve(flowmeter)

        # Check for stop signal
        if flowmeter.thisPour > 0.0 or flowmeter.totalPour > 0.0:
            if not current_used_flowmeter:
                current_used_flowmeter = pin
            if customer_id:
                totalPour = flowmeter.totalPour
                new_customer_balance -= flowmeter.thisPour * flowmeter.price
                last_acton_time = max(last_acton_time, flowmeter.lastClick)
                if balance_limit > new_customer_balance:
                    lock_valve(flowmeter)

                # Send flow
                if current_time - flowmeter.lastClick > 7000:
                    if flowmeter.clicks > 2:
                        send_flow(pin)
                        customer_balance = new_customer_balance
                        flowmeter.clear()

                flowmeter.thisPour = 0.0

    flow_string = u'В бокале: {:.3f}л'.format(totalPour)
    balance_string = u'На счете:{}{:.1f}р'.format((u'', u' ')[new_customer_balance < 1000.0], new_customer_balance)

    if balance_limit > new_customer_balance:
        flow_string = u'Пополните счет'

    if not customer_id or current_time - last_acton_time > 12000:
        new_lcd_string = u'Вставьте карту'
        customer_id = None
        customer_balance = None
        current_used_flowmeter = 0
    elif customer_id == '0':
        new_lcd_string = u'Карта не \nзарегистрирована'
    else:
        new_lcd_string = balance_string + '\n' + flow_string

    if lcd_string != new_lcd_string and current_time - last_lcd_update_time > 200:
        last_lcd_update_time = current_time
        lcd_string = new_lcd_string
        # единичное использование иногда приводит к отображению мусора
        lcd.clear()
        lcd.message(from_cyrillic(lcd_string))

    receiver_event.set()
    while receiver.is_message_come():
        message = receiver.get_message()
        message_split = message.split()
        message_type = int(message_split[0])

        if message_type == config.get_message_type('MSG_PRICES'):
            message_parsed = ast.literal_eval(message[2:])
            logging.debug("PRICES COME " + str(message_parsed))
            prices_loaded = True
            for single_row in message_parsed:
                if single_row[0] in flowmeters:
                    flowmeter = flowmeters[single_row[0]]
                    flowmeter.set_valve_GPIO_pin(single_row[1])
                    flowmeter.set_price(single_row[2])
                    flowmeter.set_maintenance(single_row[3])
        elif message_type == config.get_message_type('MSG_BALANCE'):
            logging.debug("BALANCE COME " + ' '.join(message_split))
            if current_used_flowmeter:
                if flowmeters[current_used_flowmeter].clicks > 2:
                    send_flow(current_used_flowmeter)
                flowmeters[current_used_flowmeter].clear()
            customer_balance = float(message_split[1])
            new_customer_balance = customer_balance
            customer_id = message_split[2]
            for pin in flowmeters:
                flowmeter = flowmeters[pin]
                if balance_limit < customer_balance:
                    unlock_valve(flowmeter)
                else:
                    lock_valve(flowmeter)
            last_acton_time = current_time
        else:
            logging.debug(message)
